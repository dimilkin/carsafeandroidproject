package com.telerikacademi.carsafe.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.telerikacademi.carsafe.R;
import com.telerikacademi.carsafe.databinding.ActivityPolicyInfoBinding;
import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.navigation.ActivityNavigation;
import com.telerikacademi.carsafe.navigation.NavigationProviderImpl;
import com.telerikacademi.carsafe.services.PoliciesService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PolicyInfoActivity extends AppCompatActivity {

    private static final String EXTRA_ID = "com.telerikacademi.carsafe.EXTRA_ID";
    private static final String EXTRA_APPROVAL_STATUS = "com.telerikacademi.carsafe.EXTRA_APPROVAL_STATUS";
    private static final String EXTRA_POLICY_PRICE = "com.telerikacademi.carsafe.EXTRA_POLICY_PRICE";
    private static final String EXTRA_START_DATE = "com.telerikacademi.carsafe.EXTRA_START_DATE";
    private static final String EXTRA_START_TIME = "com.telerikacademi.carsafe.EXTRA_START_TIME";
    private static final String EXTRA_VEHICLE_MODEL = "com.telerikacademi.carsafe.EXTRA_VEHICLE_MODEL";
    private static final String EXTRA_VEHICLE_BRAND = "com.telerikacademi.carsafe.EXTRA_VEHICLE_BRAND";
    private static final String EXTRA_VEHICLE_REG_DATE = "com.telerikacademi.carsafe.EXTRA_VEHICLE_REG_DATE";
    private static final String EXTRA_VEHICLE_CUBIC_CAPACITY = "com.telerikacademi.carsafe.EXTRA_VEHICLE_CUBIC_CAPACITY";
    private static final String FIRST_NAME = "com.telerikacademi.carsafe.FIRST_NAME";
    private static final String LAST_NAME = "com.telerikacademi.carsafe.LAST_NAME";
    private static final String BIRTH_DATE = "com.telerikacademi.carsafe.BIRTH_DATE";
    private static final String ACCIDENTS = "com.telerikacademi.carsafe.ACCIDENTS";

    private static final int PENDING_POLICY_STATUS = 0;

    private PoliciesService policiesService;
    private Button withdrawBtn;
    private ProgressBar progressBar;
    RelativeLayout policyRelativeLayout;
    ActivityNavigation navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityPolicyInfoBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_policy_info);
        navigation = new NavigationProviderImpl();
        SharedPreferences prefs = this.getSharedPreferences("PolicyInfo", Context.MODE_PRIVATE);
        Intent intent = getIntent();


        // show or hide UI elements ->
        progressBar = binding.policyProgressBar;
        progressBar.setVisibility(View.GONE);

        withdrawBtn = binding.activityPolicyWithdrawBtn;
        if (showWithdrawButton(intent.getIntExtra(EXTRA_APPROVAL_STATUS, -1))) {
            withdrawBtn.setVisibility(View.VISIBLE);
        } else {
            withdrawBtn.setVisibility(View.GONE);
        }

        // user details ->
        binding.acticityPolicyIdentity.setText(Integer.toString(intent.getIntExtra(EXTRA_ID, -1)) + " / " + intent.getStringExtra(EXTRA_START_DATE));
        binding.activityPolicyFirstName.setText(prefs.getString(FIRST_NAME, "No First Name Found"));
        binding.activityPolicyLastName.setText(prefs.getString(LAST_NAME, "No Last Name Found"));
        binding.activityPolicyBirthDate.setText(prefs.getString(BIRTH_DATE, "No Birth Date Found"));
        binding.activityPolicyAccidentsText.setText(prefs.getString(ACCIDENTS, "No info found"));

        // policy details ->
        binding.activityPolicyVehicleBrand.setText(intent.getStringExtra(EXTRA_VEHICLE_BRAND));
        binding.activityPolicyVehicleModel.setText(intent.getStringExtra(EXTRA_VEHICLE_MODEL));
        binding.activityPolicyRegDate.setText(intent.getStringExtra(EXTRA_VEHICLE_REG_DATE));
        binding.activityPolicyCubicCap.setText(intent.getStringExtra(EXTRA_VEHICLE_CUBIC_CAPACITY));

        binding.activityPolicyStartDate.setText(intent.getStringExtra(EXTRA_START_DATE));
        binding.activityPolicyStartTime.setText(intent.getStringExtra(EXTRA_START_TIME));
        binding.activityPolicyStatus.setText(policyStatus(intent.getIntExtra(EXTRA_APPROVAL_STATUS, -1)));
        binding.activityPolicyFinalPrice.setText(Double.toString(intent.getDoubleExtra(EXTRA_POLICY_PRICE, 0.00)));

        policiesService = PoliciesService.getInstance();
        policyRelativeLayout = binding.policyRelativeLayout;


        binding.activityPolicyBackBtn.setOnClickListener(v -> {
            navigation.navigateToActivity(this, MainActivity.class);
        });

        withdrawBtn.setOnClickListener(v -> {

            withdrawBtn.setVisibility(View.GONE);
            policyRelativeLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            SharedPreferences preferences = this.getSharedPreferences("CarSafe", Context.MODE_PRIVATE);
            String authToken = "Bearer " + preferences.getString("AUTHTOKEN", "AuthToken missing");
            int policyId = intent.getIntExtra(EXTRA_ID, -1);
            policiesService.withdrawPendingPolicy(policyId, authToken).enqueue(new Callback<PolicyInfoDto>() {
                @Override
                public void onResponse(Call<PolicyInfoDto> call, Response<PolicyInfoDto> response) {

                }

                @Override
                public void onFailure(Call<PolicyInfoDto> call, Throwable t) {

                }
            });

            Toast.makeText(PolicyInfoActivity.this, "Policy status changed to withdrawn!", Toast.LENGTH_LONG).show();
            new Handler().postDelayed(this::back, 1000);
        });
    }

    private void back(){
        navigation.navigateToActivity(this, MainActivity.class);
    }

    private boolean showWithdrawButton(int policyStatus) {
        return policyStatus == PENDING_POLICY_STATUS;
    }

    private String policyStatus(int approvalStatus) {
        switch (approvalStatus) {
            case 0:
                return "Pending";
            case 1:
                return "Approved";
            case 2:
                return "Declined";
            case 3:
                return "Withdrawn";
            default:
                return "No Info Available";
        }
    }
}