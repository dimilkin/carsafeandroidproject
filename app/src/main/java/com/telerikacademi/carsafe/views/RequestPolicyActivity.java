package com.telerikacademi.carsafe.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.telerikacademi.carsafe.R;
import com.telerikacademi.carsafe.databinding.ActivityRequestPolicyBinding;
import com.telerikacademi.carsafe.models.CarBrand;
import com.telerikacademi.carsafe.models.CarModel;
import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.navigation.ActivityNavigation;
import com.telerikacademi.carsafe.navigation.NavigationProviderImpl;
import com.telerikacademi.carsafe.services.PoliciesService;
import com.telerikacademi.carsafe.viewmodels.RequestPolicyViewModel;
import com.telerikacademi.carsafe.views.utils.DatePickerFragment;
import com.telerikacademi.carsafe.views.utils.TimePickerFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestPolicyActivity extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private static final String EXTRA_TOKEN = "com.telerikacademi.carsafe.EXTRA_TOKEN";
    private static final String EXTRA_VEHICLE_REG_DATE = "com.telerikacademi.carsafe.EXTRA_VEHICLE_REG_DATE";
    private static final String EXTRA_VEHICLE_CUBIC_CAPACITY = "com.telerikacademi.carsafe.EXTRA_VEHICLE_CUBIC_CAPACITY";
    private static final String EXTRA_VEHICLE_MODEL = "com.telerikacademi.carsafe.EXTRA_VEHICLE_MODEL";
    private static final String EXTRA_VEHICLE_BRAND = "com.telerikacademi.carsafe.EXTRA_VEHICLE_BRAND";
    private static final Integer NEW_OFFER_REQUEST_CODE = 1;

    private Spinner brandSpiner, modelSpiner, carCubics;
    private Button requestPolicyOfferBtn, backBtn;
    private TextView vehicleRegDateTextField, policyStartDateTextFIeld, policyStartTimeTextField;
    private List<CarBrand> carBrandsList;
    private List<CarModel> carModelsList;
    private List<Integer> carCubicsList;
    private LinearLayout policyStartDateLayout;
    private boolean settingRegistrationDate;
    private PolicyInfoDto policyInfoDto;
    private ActivityNavigation navigation;
    private ProgressBar progressBar;

    private RequestPolicyViewModel requestViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityRequestPolicyBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_request_policy);
        brandSpiner = binding.chooseBrand;
        modelSpiner = binding.chooseModel;
        carCubics = binding.chooseCubicCapacity;
        policyStartDateLayout = binding.policyStartTimeLayout;
        vehicleRegDateTextField = binding.requestPolicyRegDateTextView;
        policyStartDateTextFIeld = binding.requestPolicytartDateTextField;
        policyStartTimeTextField = binding.requestPolicytartTimeTextField;
        requestPolicyOfferBtn = binding.requestPolicySaveBtn;
        progressBar = binding.requestPolicyProgressBar;
        backBtn = binding.requestPolicyBackBtn;

        navigation = new NavigationProviderImpl();

        progressBar.setVisibility(View.GONE);
        policyStartDateLayout.setVisibility(View.GONE);
        requestPolicyOfferBtn.setVisibility(View.GONE);

        settingRegistrationDate = true;
        policyInfoDto = new PolicyInfoDto();

        carBrandsList = new ArrayList<>();
        carModelsList = new ArrayList<>();
        carCubicsList = new ArrayList<>();

        Intent intent = getIntent();
        String authToken = intent.getStringExtra(EXTRA_TOKEN);


        requestViewModel = ViewModelProviders.of(this).get(RequestPolicyViewModel.class);
        requestViewModel.getCarBrands(authToken);
        observeCarBrands();

        brandSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CarBrand brand = (CarBrand) parent.getSelectedItem();
                policyInfoDto.setVechicleBrand(brand.getBrand());

                requestViewModel.getCarModelsForBrand(authToken, brand.getId());
                observeCarModels();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        modelSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CarModel model = (CarModel) parent.getSelectedItem();
                policyInfoDto.setVechicleModel(model.getModel());
                requestViewModel.getMaxCarCubics(authToken);
                observeCarCubics();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        carCubics.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Integer cub = (Integer) parent.getSelectedItem();
                policyInfoDto.setVehicleCubicCapacity(0 + " - " + cub);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.requestPolicyRegDateSelectBtn.setOnClickListener(v -> {
            showDatePickerDialog();
        });

        binding.requestPolicytartDateSelectBtn.setOnClickListener(v -> {
            showDatePickerDialog();
        });

        binding.requestPolicytartTimeSelectBtn.setOnClickListener(v -> {
            showTimePickerDialog();
        });

        requestPolicyOfferBtn.setOnClickListener(v -> {
            requestPolicyOfferBtn.setVisibility(View.GONE);
            Intent offerIntent = new Intent(RequestPolicyActivity.this, OfferReceivingActivity.class);
            offerIntent.putExtra(EXTRA_VEHICLE_CUBIC_CAPACITY, policyInfoDto.getVehicleCubicCapacity());
            offerIntent.putExtra(EXTRA_VEHICLE_REG_DATE, policyInfoDto.getVehicleRegDate());
            offerIntent.putExtra(EXTRA_VEHICLE_BRAND, policyInfoDto.getVechicleBrand());
            offerIntent.putExtra(EXTRA_VEHICLE_MODEL, policyInfoDto.getVechicleModel());
            offerIntent.putExtra(EXTRA_TOKEN, authToken);

            startActivityForResult(offerIntent, NEW_OFFER_REQUEST_CODE);
        });

        backBtn.setOnClickListener(v -> {
          back();
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_OFFER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                progressBar.setVisibility(View.VISIBLE);
                policyStartDateLayout.setVisibility(View.GONE);
                backBtn.setVisibility(View.GONE);
                Double resultOfferPrice = data.getDoubleExtra("resultPrice", 0.00);
                String authToken = data.getStringExtra("authToken");
                policyInfoDto.setPolicyPrice(resultOfferPrice);
                PoliciesService policiesService = PoliciesService.getInstance();
                policiesService.requestNewPolicy(authToken, policyInfoDto).enqueue(new Callback<PolicyInfoDto>() {
                    @Override
                    public void onResponse(Call<PolicyInfoDto> call, Response<PolicyInfoDto> response) {

                    }

                    @Override
                    public void onFailure(Call<PolicyInfoDto> call, Throwable t) {

                    }
                });
                Toast.makeText(RequestPolicyActivity.this, "Policy offer has been submitted!", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(this::back, 2000);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(RequestPolicyActivity.this, "No request has been submitted!", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(this::back, 1000);
            }
        }
    }

    private void observeCarBrands() {
        requestViewModel.carBrandsList.observe(this, carBrands -> {
            if (carBrands != null) {
                carBrandsList.addAll(carBrands);

                ArrayAdapter<CarBrand> brandArrayAdapter = generateAdapter(carBrandsList);
                brandArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                brandSpiner.setAdapter(brandArrayAdapter);

            }
        });
    }

    private void observeCarModels() {
        requestViewModel.carModelsList.observe(this, carModels -> {
            if (carModels != null) {
                carModelsList.clear();
                carModelsList.addAll(carModels);

                ArrayAdapter<CarModel> modelsArrayAdapter = generateAdapter(carModelsList);
                modelsArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                modelSpiner.setAdapter(modelsArrayAdapter);


            }
        });
    }


    private void observeCarCubics() {
        requestViewModel.carCubicsList.observe(this, listOfCubics -> {
            if (listOfCubics != null) {
                carCubicsList.clear();
                carCubicsList.addAll(listOfCubics);

                ArrayAdapter<Integer> cubicsArrayAdapter = generateAdapter(carCubicsList);
                cubicsArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                carCubics.setAdapter(cubicsArrayAdapter);
            }
        });
    }

    private <E> ArrayAdapter<E> generateAdapter(List<E> list) {
        return new ArrayAdapter<E>(this, android.R.layout.simple_spinner_item, list);

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (settingRegistrationDate) {
            vehicleRegDateTextField.setTextSize(20);
            vehicleRegDateTextField.setText(dayOfMonth + "." + month + "." + year);

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            Date date = calendar.getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String regDate = formatter.format(date);
            policyInfoDto.setVehicleRegDate(regDate);
            settingRegistrationDate = false;
            policyStartDateLayout.setVisibility(View.VISIBLE);


        } else {
            policyStartDateTextFIeld.setTextSize(18);
            policyStartDateTextFIeld.setText(dayOfMonth + "." + month + "." + year);

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            Date date = calendar.getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String startDate = formatter.format(date);
            policyInfoDto.setStartDate(startDate);
            settingRegistrationDate = true;
            requestPolicyOfferBtn.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        policyStartTimeTextField.setTextSize(18);
        policyStartTimeTextField.setText(hourOfDay + " : " + minute);
        String startTime = hourOfDay + ":" + minute;
        policyInfoDto.setStartTime(startTime);
    }

    private void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    private void back() {
        navigation.navigateToActivity(this, MainActivity.class);
    }
}