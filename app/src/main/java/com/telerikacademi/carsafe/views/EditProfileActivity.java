package com.telerikacademi.carsafe.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.telerikacademi.carsafe.R;
import com.telerikacademi.carsafe.databinding.ActivityEditProfileBinding;
import com.telerikacademi.carsafe.models.dto.UserProfileInfoDto;
import com.telerikacademi.carsafe.navigation.ActivityNavigation;
import com.telerikacademi.carsafe.navigation.NavigationProviderImpl;
import com.telerikacademi.carsafe.services.UserInfoService;
import com.telerikacademi.carsafe.views.utils.DatePickerFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static final String EXTRA_ID = "com.telerikacademi.carsafe.EXTRA_ID";
    private static final String EXTRA_TOKEN = "com.telerikacademi.carsafe.EXTRA_TOKEN";

    private CheckBox accidentCheckBox;
    private String time;
    private UserInfoService userInfoService;
    private TextView birthDate;
    private ActivityNavigation navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityEditProfileBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        userInfoService = UserInfoService.getInstance();
        binding.profileUpdateProgressBar.setVisibility(View.GONE);
        navigation = new NavigationProviderImpl();
        accidentCheckBox = binding.editProfileAccidentCheckBox;
        boolean hadAccidentLastYear = false;
        if (accidentCheckBox.isChecked()) {
            hadAccidentLastYear = true;
        }

        String hadAccidentsLastYearToString = hadAccidentLastYear ? "Yes" : "No";

        binding.editProfileBirthdateSelectBtn.setOnClickListener(v -> {
            showDatePickerDialog();
        });

        birthDate = binding.editProfileBirthdateText;

        Intent intent = getIntent();

        binding.editProfileSaveBtn.setOnClickListener(btn -> {
            int policyId = intent.getIntExtra(EXTRA_ID, -1);
            String authToken = intent.getStringExtra(EXTRA_TOKEN);

            UserProfileInfoDto userInfo = new UserProfileInfoDto();
            userInfo.setFirstname(textResult(binding.editProfFirstNameTextField));
            userInfo.setLastname(textResult(binding.editProfLastNameTextField));
            userInfo.setPhoneNumber(textResult(binding.editProfPhoneTextField));
            userInfo.setAddress(textResult(binding.editProfAddressTextField));
            userInfo.setAccidentLastYear(hadAccidentsLastYearToString);
            userInfo.setBirthdate(time);

            binding.editProfileSaveBtn.setVisibility(View.GONE);
            binding.profileUpdateProgressBar.setVisibility(View.VISIBLE);

            userInfoService.updateUserProfileInfo(authToken, policyId, userInfo).enqueue(new Callback<UserProfileInfoDto>() {
                @Override
                public void onResponse(Call<UserProfileInfoDto> call, Response<UserProfileInfoDto> response) {

                }

                @Override
                public void onFailure(Call<UserProfileInfoDto> call, Throwable t) {

                }
            });


            Toast.makeText(EditProfileActivity.this, "Profile Changed Succesfuly!", Toast.LENGTH_LONG).show();
            new Handler().postDelayed(this::back, 1000);

        });
    }

    private void back() {
        navigation.navigateToActivity(this, MainActivity.class);
    }

    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        birthDate.setText(day + "-" + month + "-" + year);
        birthDate.setTextSize(18);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        Date date = calendar.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String birthDate = formatter.format(date);
        time = birthDate;
    }

    private void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private String textResult(EditText editText) {
        return editText.getText().toString();
    }
}