package com.telerikacademi.carsafe.services;

import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.network.NetworkProvider;

import io.reactivex.Single;
import retrofit2.Call;

public class PoliciesService {

    private static PoliciesService instance;
    private NetworkProvider networkProvider;

    private PoliciesService() {
        networkProvider = new NetworkProvider();
    }

    public static PoliciesService getInstance() {
        if (instance == null) {
            instance = new PoliciesService();
        }
        return instance;
    }

    public Call<PolicyInfoDto> withdrawPendingPolicy(int policyId, String authToken){
        return networkProvider.getConnection().withdrawPendingPolicy(authToken, policyId);
    }

    public Call<PolicyInfoDto> requestNewPolicy(String token, PolicyInfoDto infoDto){
        return networkProvider.getConnection().registerNewPolicy(token, infoDto);
    }

    public Single<Double> getOfferPrice(String token, PolicyInfoDto infoDto){
        return networkProvider.getConnection().getOfferPrice(token, infoDto);
    }

}
