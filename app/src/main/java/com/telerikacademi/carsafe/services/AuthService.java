package com.telerikacademi.carsafe.services;

import com.telerikacademi.carsafe.models.dto.NetworkResponse;
import com.telerikacademi.carsafe.models.dto.RegistrationInfo;
import com.telerikacademi.carsafe.network.NetworkProvider;

import io.reactivex.Single;
import retrofit2.Call;


public class AuthService {

    private static AuthService instance;
    private NetworkProvider networkProvider;

    private AuthService() {
        networkProvider = new NetworkProvider();
    }

    public static AuthService getAuthInstance() {
        if (instance == null) {
            instance = new AuthService();
        }
        return instance;
    }

    public Call<NetworkResponse> authenticate(String email, String password) {
        return networkProvider.getConnection().authenticate(email, password);
    }

    public Call<RegistrationInfo> registerNewUser(RegistrationInfo registrationInfo){
        return networkProvider.getConnection().registerNewUser(registrationInfo);
    }

}
