package com.telerikacademi.carsafe.models.dto;

public class NetworkResponse {

    private int token;

    public NetworkResponse() {
    }

    public int getUserId() {
        return token;
    }

    public void setUserId(int userId) {
        this.token = userId;
    }
}
