package com.telerikacademi.carsafe.models;

import java.util.Set;

public class Policy {

    private int id;

    private String startDate;

    private String startTime;

    private Double totalPrice;

    private byte approval;

    private UserInfo owner;

    private Car insuredCar;

    private Set<Image> policyImageSet;

    public Policy() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public byte getApproval() {
        return approval;
    }

    public void setApproval(byte approval) {
        this.approval = approval;
    }

    public UserInfo getOwner() {
        return owner;
    }

    public void setOwner(UserInfo owner) {
        this.owner = owner;
    }

    public Car getInsuredCar() {
        return insuredCar;
    }

    public void setInsuredCar(Car insuredCar) {
        this.insuredCar = insuredCar;
    }

    public Set<Image> getPolicyImageSet() {
        return policyImageSet;
    }

    public void setPolicyImageSet(Set<Image> policyImageSet) {
        this.policyImageSet = policyImageSet;
    }
}
