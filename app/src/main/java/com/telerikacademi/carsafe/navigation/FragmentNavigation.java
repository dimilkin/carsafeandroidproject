package com.telerikacademi.carsafe.navigation;

import androidx.fragment.app.Fragment;

public interface FragmentNavigation {

    void navigateToFragment(Fragment fragment, boolean addToBackstack);

}
